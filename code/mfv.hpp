﻿#pragma once

#include <string>
#include <vector>
#include <stdio.h>

#define GLM_FORCE_PURE
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include "glm/glm.hpp"

namespace mfv
{
	class model
	{
	public:
		enum stride
		{
			str_vertex,
			str_color,
			str_uv,
			str_normal,
			str_weight,
			stride_max
		};
		struct vertex
		{
			float pos_x;
			float pos_y;
			float pos_z;
			float pos_w;

			float normal_x;
			float normal_y;
			float normal_z;

			float u;
			float v;
		};

	private:
		struct weight
		{
			std::vector<float> weight;
			std::vector<unsigned int> index;
		};
		struct animation
		{
			glm::mat4 init;
			std::vector<glm::mat4> frame;
		};
		struct bone
		{
			std::vector<weight> weights;
			std::vector<animation> index;
		};
		struct material
		{
			std::string name;
		};
		struct mash
		{
			int vertex_size;
			std::vector<float> vertexs; //xyzw normal uv の順で頂点順に並んでる
			std::vector<vertex> vertex_arry;
			std::vector<bone> bones;
			int material_index;
		};
	public:
		int stride_size;
		std::vector<stride> strides;
		std::vector<mash> meshs;
		std::vector<material> materials;
		bool save_file(std::string file_name)
		{
			//ファイルを確保
			FILE* fp = NULL;
			if ((fp = fopen(file_name.data(), "wb")) == NULL)
			{
				printf("ファイル(%s)が開けませんでした\n", file_name.data());
				return true;
			}

			//型の形を書き込む
			fwrite(&this->stride_size, sizeof(int), 1, fp);
			int stridesize = (int)this->strides.size();
			fwrite(&stridesize, sizeof(int), 1, fp);
			fwrite(&this->strides[0], sizeof(int), stridesize, fp);

			//meshを書き込み
			int mesh_size = (int)this->meshs.size();
			fwrite(&mesh_size, sizeof(int), 1, fp);
			for (size_t i = 0; i < (size_t)mesh_size; i++)
			{
				int vertexsize = this->meshs[i].vertex_size;
				fwrite(&vertexsize, sizeof(int), 1, fp);
				fwrite(&this->meshs[i].vertex_arry[0], sizeof(vertex), vertexsize, fp);
				int vertex_arrey_size = (int)this->meshs[i].vertexs.size();
				fwrite(&vertex_arrey_size, sizeof(int), 1, fp);
				fwrite(this->meshs[i].vertexs.data(), sizeof(float), vertex_arrey_size, fp);
			}
			int material_size = (int)this->materials.size();
			fwrite(&material_size, sizeof(int), 1, fp);
			for (int i = 0; i < material_size; i++)
			{
				fwrite(this->materials[i].name.data(), sizeof(char), MAX_PATH, fp);
			}
			
			fclose(fp);
			return false;
		};
		bool load_file(std::string file_name)
		{
			//ファイルの読み込み
			FILE* fp = NULL;
			if ((fp = fopen(file_name.data(), "rb")) == NULL)
			{
				printf("ファイル(%s)が開けませんでした\n", file_name.data());
				return true;
			}

			//型を読み込み
			fread(&this->stride_size, sizeof(int), 1, fp);
			int stridesize;
			fread(&stridesize, sizeof(int), 1, fp);
			this->strides.resize(stridesize);
			fread(&this->strides[0], sizeof(int), stridesize, fp);

			//meshを読み込み
			int mesh_size;
			fread(&mesh_size, sizeof(int), 1, fp);
			this->meshs.resize(mesh_size);
			for (int i = 0; i < mesh_size; i++)
			{
				int vertexsize;
				fread(&vertexsize, sizeof(int), 1, fp);
				this->meshs[i].vertex_size = vertexsize;
				this->meshs[i].vertex_arry.resize(vertexsize);

				fread(&this->meshs[i].vertex_arry[0], sizeof(vertex), vertexsize, fp);
				fread(&vertexsize, sizeof(int), 1, fp);
				this->meshs[i].vertexs.resize(vertexsize);
				fread(this->meshs[i].vertexs.data(), sizeof(float), this->meshs[i].vertexs.size(), fp);


			}
			int material_size = 0;
			fread(&material_size, sizeof(int), 1, fp);
			this->materials.resize(material_size);
			for (int i = 0; i < material_size; i++)
			{
				char* name = new char[MAX_PATH];
				fread(name, sizeof(char), MAX_PATH, fp);
				this->materials[i].name.append(name);
				delete[] name;
			}
			fclose(fp);
			return false;
		};
	};
}

