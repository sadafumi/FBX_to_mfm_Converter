#include "pch.h"

fbxsdk::FbxTime g_period;
int g_nStartFrame;
int g_nStopFrame;
bool g_bAnim;//アニメーションがあるかどうか

bool Get_Fbx_Animation(FbxScene* scene, mfm::model* Mesh)
{
	FbxArray<FbxString*> anim_take_name_array;
	scene->FillAnimStackNameArray(anim_take_name_array);
	// アニメーションの有無をチェック
	int take_max_num = anim_take_name_array.GetCount();
	Message_Stack::Set << "アニメーション数[" << take_max_num << "]\n";
	Mesh->animations.resize(take_max_num);
	FbxTime delta_time;
	delta_time.SetTime(0, 0, 0, 1, 0, scene->GetGlobalSettings().GetTimeMode());
	for (int i = 0; i < take_max_num; ++i)
	{
		FbxTakeInfo* info = scene->GetTakeInfo(*(anim_take_name_array[i]));
		Get_Time(&Mesh->animations[i], info, delta_time);
		FbxAnimStack* animation = scene->FindMember<FbxAnimStack>(Mesh->animations[i].name.data());
		scene->SetCurrentAnimationStack(animation);
		auto* mesh = scene->GetSrcObject<FbxMesh>(0);
		Get_Animation(&Mesh->animations[i], mesh);
	}
	return false;
}

bool Get_Time(mfm::animation* out_animation, FbxTakeInfo* in_info, FbxTime delta_time)
{
	if (in_info)
	{
		Set_Name(out_animation, in_info);
		Message_Stack::Set  << "アニメーション:フレーム [" << out_animation->name << "]";
		out_animation->frame = Set_Frame(in_info, delta_time);		
	}
	return false;
}

void Set_Name(mfm::animation* out_animation, FbxTakeInfo* in_info)
{
	std::string anim_name = (const char*)in_info->mName;
	out_animation->name.append(anim_name);
}

int Set_Frame(FbxTakeInfo* in_info, FbxTime delta_time)
{
	fbxsdk::FbxTime start = in_info->mLocalTimeSpan.GetStart();//アニメーションが始まる時間
	fbxsdk::FbxTime stop = in_info->mLocalTimeSpan.GetStop();//終わる時間
	g_nStartFrame = (int)(start.Get() / delta_time.Get());//1フレームあたりで割れば、フレーム数になります
	g_nStopFrame = (int)(stop.Get() / delta_time.Get());
	Message_Stack::Set  << " [" << g_nStartFrame << "]~[" << g_nStopFrame << "]\n";
	return  g_nStopFrame - g_nStartFrame + 1;
}

bool Get_Animation(mfm::animation* out_animation, FbxMesh* inMesh)
{
	FbxSkin* skin = (FbxSkin*)inMesh->GetDeformer(0, FbxDeformer::eSkin);
	int cluster_size = skin->GetClusterCount();
	out_animation->matrixs.resize(cluster_size);
	for (int i = 0; i < cluster_size; i++)
	{
		FbxCluster* bone = skin->GetCluster(i);
		FbxAMatrix initMat;
		bone->GetTransformLinkMatrix(initMat);
		out_animation->matrixs[i].frame.resize(out_animation->frame);
		Set_Matrix(&out_animation->matrixs[i], out_animation->frame,bone);
	}
	return false;
}

void Set_Matrix(mfm::animation_list* in_matrix, int frame,FbxCluster* in_bone)
{
	//初期姿勢を取得
	FbxAMatrix initMat;
	in_bone->GetTransformLinkMatrix(initMat);
	//行列セット
	for (int r = 0; r < 4; r++)
	{
		for (int c = 0; c < 4; c++)
		{
			in_matrix->init.set(r, c, (float)initMat.Get(r, c));
		}
	}

	for (int i = 0; i < frame; ++i)
	{
		FbxAMatrix mat;
		FbxTime time = (g_nStartFrame + i) * g_period.Get();
		mat = in_bone->GetLink()->EvaluateGlobalTransform(time);

		for (int r = 0; r < 4; r++)
		{
			for (int c = 0; c < 4; c++)
			{
				in_matrix->frame[i].set(r, c, (float)mat.Get(r, c));
			}
		}
	}
}

//void getAnimation(mfm::Skeleton*InBone, FbxCluster *cluster, int Id)
//{
//
//	//初期姿勢を取得
//	FbxAMatrix initMat;
//	cluster->GetTransformLinkMatrix(initMat);
//
//	int frameCount = g_nStopFrame - g_nStartFrame + 1;
//	InBone->Animations[Id].Matrixs.resize(frameCount);
//
//	//行列セット
//	for (int r = 0; r < 4; r++)
//	{
//		for (int c = 0; c < 4; c++)
//		{
//			InBone->Animations[Id].init.Set(r, c, (float)initMat.Get(r, c));
//		}
//	}
//
//	for (int i = 0; i < frameCount; ++i)
//	{
//		FbxAMatrix mat;
//		FbxTime time = (g_nStartFrame + i) * g_period.Get();
//		mat = cluster->GetLink()->EvaluateGlobalTransform(time);
//		//openglとdirectxの行列の変換って要るのか！？とりあえずやってみる
//
//		for (int r = 0; r < 4; r++)
//		{
//			for (int c = 0; c < 4; c++)
//			{
//				InBone->Animations[Id].Matrixs[i].Set(r, c, (float)mat.Get(r, c));
//			}
//		}
//	}
//}
//
