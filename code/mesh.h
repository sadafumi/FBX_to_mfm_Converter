#pragma once
#include <fbxsdk.h>
#include "mfm.hpp"

bool Get_Fbx_Mesh(FbxScene* scene, mfm::model* Mesh);
bool Get_Fbx_Bones(FbxScene* scene, FbxMesh* inMesh, mfm::mesh* Mesh);
bool Get_Bone_Data(FbxSkin* in_skin, mfm::mesh* Mesh);
bool Get_Point_Data(FbxCluster* in_bone, mfm::mesh* Mesh, int cluster_index);
bool Set_Mesh_Date(mfm::mesh* InMesh, FbxMesh* FbxMesh);