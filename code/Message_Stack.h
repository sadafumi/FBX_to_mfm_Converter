#pragma once
#include <string>
#include <vector>
#include <Thread>
#include <queue>
#include <iostream>
#include <sstream>
#include <streambuf>
#include <cstring>
#include <mutex>

class Setter;
class Message_Stack
{
public:
	void Start();
	void Save_Log();
	static void SetMessage(std::string);
	static Setter Set;
	void End();
private:
	void Main_Process();
	std::vector<std::string> Stack;
	std::thread thread;
	static std::queue<std::string> Set_Queue;
	static std::mutex mtx;
	const char* Log_name = "Log.txt";
};
class Message_Stream_Buffer : public std::streambuf
{
	char Buffer[2];
public:
	Message_Stream_Buffer() 
	{
		std::memset(Buffer,0, sizeof Buffer);
	}
	virtual int_type overflow(int_type iChar = EOF)
	{
		if (iChar != EOF)
		{
			Buffer[0] = (char)iChar;
			std::string Buff(Buffer);
			Message_Stack::SetMessage(Buff);
		}
		return iChar;
	}
};
class Setter : public std::ostream
{
	Message_Stream_Buffer* Message_buffer;
public:
	~Setter() { delete Message_buffer;}
	Setter() : std::ostream(Message_buffer = new Message_Stream_Buffer) {}
};
