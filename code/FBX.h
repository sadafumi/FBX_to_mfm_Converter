#pragma once
#include <fbxsdk.h>

class FBX
{
public:
	static bool load_fbx(std::string* flie_link, mfm::model* mesh);
private:
	static bool CreateFbxManager(FbxManager** manager);
	static bool CreateFbxImporter(FbxManager** manager, FbxImporter** importer);
	static bool CreateFbxScene(FbxManager** manager, FbxScene** scene);
};

