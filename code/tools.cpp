#include "pch.h"

void tools::cat_file_path(std::string* Out, std::string In)
{
	char drive[MAX_PATH + 1]
		, dir[MAX_PATH + 1]
		, fname[MAX_PATH + 1]
		, ext[MAX_PATH + 1];

	_splitpath(In.data(), drive, dir, fname, ext);//パス名を構成要素に分解します
	std::string drive_cahr(drive);
	std::string dir_cahr(dir);
	Out->append(drive_cahr + dir_cahr);
}

void tools::get_file_name(std::string* out, std::string* in)
{
	char drive[MAX_PATH + 1]
		, dir[MAX_PATH + 1]
		, fname[MAX_PATH + 1]
		, ext[MAX_PATH + 1];

	_splitpath(in->data(), drive, dir, fname, ext);//パス名を構成要素に分解します
	std::string name_cahr(fname);
	std::string extcahr(ext);
	out->append(name_cahr + extcahr);
}

void tools::change_file_extension(std::string* Out, std::string* In, std::string* Extension)
{
	int PeriodPos = 0;
	std::string buff(*In);
	PeriodPos = (int)In->find_last_of(".");
	buff.replace(PeriodPos + 1, Extension->size(), Extension->data());
	Out->append(buff);
}

std::string tools::path_editer(std::string* File_dir, std::string* Full_path, const char* SetExtension)
{
	std::string out_path;
	std::string out_put_address;
	std::string file_name;
	std::string Extension(SetExtension);

	change_file_extension(&out_put_address, Full_path, &Extension);
	get_file_name(&file_name, &out_put_address);
	std::string out_File_name(file_name);
	std::string buff("\\");

	out_path.append(File_dir->data() + buff + out_File_name);
	return out_path;
}

void tools::check_export_folder(std::string* out_path, std::string* out_dir,const char* FolderName)
{
	std::string out_Fileder_name(FolderName);
	std::string out_File_dir(*out_dir + out_Fileder_name);
	FILE* fp;
	fopen_s(&fp, out_File_dir.data(), "r");
	if (fp == NULL)
	{
		_mkdir(out_File_dir.data());
	}
	else
	{
		fclose(fp);
	}
	out_path->append(out_File_dir);
}

bool tools::zero_check(const int valu, const char* out_sritng)
{
	if (valu == 0)
	{
		Message_Stack::Set << out_sritng;
		return true;
	}
	return false;
}

void tools::change_vertex_data(mfm::model* in_data, mfv::model* out_data)
{
	if (in_data->meshs[0].vertex.size() != 0)
	{
		out_data->strides.push_back(mfv::model::str_vertex);
		out_data->stride_size += sizeof(float) * 4;
	}
	if (in_data->meshs[0].normals.size() != 0)
	{
		out_data->strides.push_back(mfv::model::str_normal);
		out_data->stride_size += sizeof(float) * 3;
	}
	if (in_data->meshs[0].uv.size() != 0)
	{
		out_data->strides.push_back(mfv::model::str_uv);
		out_data->stride_size += sizeof(float) * 2;
	}
	//if (in_data->boneData[0].VertexData.Num != 0)
	//{
	//	out_data->strides.push_back(mfv::model::Str_weight);
	//	out_data->stride_size += sizeof(float) * 2;
	//}

	out_data->meshs.resize(in_data->meshs.size());

	for (unsigned int i = 0; i < in_data->meshs.size(); i++)
	{
		out_data->meshs[i].vertex_size = (int)in_data->meshs[i].index.size();
		out_data->meshs[i].vertex_arry.resize(in_data->meshs[i].index.size());
		for (unsigned int vertex_index = 0; vertex_index < in_data->meshs[i].index.size(); vertex_index++)
		{
			int index = in_data->meshs[i].index[vertex_index];

			out_data->meshs[i].vertex_arry[vertex_index].pos_x = in_data->meshs[i].vertex[index].x;
			out_data->meshs[i].vertex_arry[vertex_index].pos_y = in_data->meshs[i].vertex[index].y;
			out_data->meshs[i].vertex_arry[vertex_index].pos_z = in_data->meshs[i].vertex[index].z;
			out_data->meshs[i].vertex_arry[vertex_index].pos_w = 1.0f;

			out_data->meshs[i].vertexs.push_back(in_data->meshs[i].vertex[index].x);
			out_data->meshs[i].vertexs.push_back(in_data->meshs[i].vertex[index].y);
			out_data->meshs[i].vertexs.push_back(in_data->meshs[i].vertex[index].z);
			out_data->meshs[i].vertexs.push_back(1.0f);

			out_data->meshs[i].vertex_arry[vertex_index].normal_x = in_data->meshs[i].normals[vertex_index].x;
			out_data->meshs[i].vertex_arry[vertex_index].normal_y = in_data->meshs[i].normals[vertex_index].y;
			out_data->meshs[i].vertex_arry[vertex_index].normal_z = in_data->meshs[i].normals[vertex_index].z;

			out_data->meshs[i].vertexs.push_back(in_data->meshs[i].normals[vertex_index].x);
			out_data->meshs[i].vertexs.push_back(in_data->meshs[i].normals[vertex_index].y);
			out_data->meshs[i].vertexs.push_back(in_data->meshs[i].normals[vertex_index].z);

			out_data->meshs[i].vertex_arry[vertex_index].u = in_data->meshs[i].uv[vertex_index].u;
			out_data->meshs[i].vertex_arry[vertex_index].v = in_data->meshs[i].uv[vertex_index].v;
			out_data->meshs[i].vertexs.push_back(in_data->meshs[i].uv[vertex_index].u);
			out_data->meshs[i].vertexs.push_back(in_data->meshs[i].uv[vertex_index].v);
		}

		out_data->meshs[i].material_index = out_data->meshs[i].material_index;
	}

	out_data->materials.resize(in_data->material.size());
	for (size_t i = 0; i < out_data->materials.size(); i++)
	{
		std::string Extension("mtf");
		out_data->materials[i].name.append(in_data->material[i].name);
		tools::change_file_extension(&out_data->materials[i].name, &in_data->material[i].name, &Extension);
	}
}
