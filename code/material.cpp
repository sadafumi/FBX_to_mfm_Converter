#include "pch.h"

bool Set_Material_Date(mfm::material* in_data, FbxSurfaceMaterial* fbx_mesh)
{
	FbxProperty prop = fbx_mesh->FindProperty(FbxSurfaceMaterial::sDiffuse);
	int num = prop.GetSrcObjectCount<FbxLayeredTexture>();
	int Texnum = prop.GetSrcObjectCount<FbxFileTexture>();
	FbxFileTexture* texture = prop.GetSrcObject<FbxFileTexture>(0);
	Message_Stack::Set << "ベース("<< Texnum <<")レイヤード("<< num <<")\n";
	if (num > 0 || Texnum > 0)
	{
		in_data->name.append(texture->GetRelativeFileName());
	}
	else
	{
		in_data->name.append("");
	}
	return false;
}
bool Get_Fbx_Material(FbxScene* scene, mfm::model* Mesh)
{
	// シーンに含まれるマテリアルの解析
	auto materialCount = scene->GetMaterialCount();
	Message_Stack::Set << "materialの数("<< materialCount <<")\n";
	Mesh->material.resize(materialCount);
	for (int i = 0; i < materialCount; ++i)
	{
		FbxSurfaceMaterial* material = scene->GetMaterial(i);
		Message_Stack::Set << "material名(" << material->GetName()<< ")\n";
	}
	for (int i = 0; i < materialCount; i++)
	{
		FbxSurfaceMaterial* material = scene->GetMaterial(i);
		Set_Material_Date(&Mesh->material[i], material);
	}
	return false;
};