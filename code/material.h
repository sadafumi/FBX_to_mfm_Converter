#pragma once
#include <fbxsdk.h>
#include "mfm.hpp"

bool Set_Material_Date(mfm::material* InMesh, FbxSurfaceMaterial* FbxMesh);
bool Get_Fbx_Material(FbxScene* scene, mfm::model* Mesh);