#include "pch.h"

void Job_Allocate::main_job(std::string path, std::string exit_path)
{
	mfm::model model;
	mfv::model Out_mfv;

	if (FBX::load_fbx(&path, &model))
	{
		Message_Stack::Set << "FBXファイルの読み込みに失敗しました\n";
		return;
	}

	tools::change_vertex_data(&model,&Out_mfv);
	{
		model.File_Save_Full_Data(tools::path_editer(&exit_path, &path, "mfm").data());
		Message_Stack::Set << "mfmファイルを正常に保存できました\n";
		Out_mfv.save_file(tools::path_editer(&exit_path, &path, "mtv").data());
		Message_Stack::Set << "mfvファイルを正常に保存できました\n";
	}
}
