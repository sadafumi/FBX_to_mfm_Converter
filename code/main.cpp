#include "pch.h"

int main(int argc, char *argv[])
{
	argc = argc;
	Message_Stack Out_Message;
	Out_Message.Start();
	
	std::cout  << "start\n";
	std::string exit_path;
	std::string exe_path(std::filesystem::current_path().string());
	exe_path.append("\\");
	tools::check_export_folder(&exit_path, &exe_path, "Exit_Model");
	
	if (argv[1] == NULL)
	{
		std::string input_path;
		tools::check_export_folder(&input_path, &exe_path, "Input_Model");
		std::filesystem::directory_iterator iter(input_path), end;
		std::error_code err;

		int file_num = 1;
		for (; iter != end && !err; iter.increment(err)) 
		{	
			if (iter->path().extension() == ".fbx")
			{
				Message_Stack::Set << "\n[ファイル="<< file_num << "]\n";
				Job_Allocate::main_job(iter->path().string(), exit_path);
				file_num++;
			}
		}
	}
	else
	{
		std::string path;
		Message_Stack::Set  << "読み込みファイル>>"<< argv[1]<< "\n";
		path.append(argv[1]);
		Job_Allocate::main_job(path, exit_path);
	}
	
	Out_Message.Save_Log();
	Out_Message.End();
	std::cout << "exit\n";
}