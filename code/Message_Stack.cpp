#include "pch.h"
//#include <fstream>  

std::queue<std::string> Message_Stack::Set_Queue;
Setter  Message_Stack::Set;
std::mutex Message_Stack::mtx;

void Message_Stack::Start()
{
	this->thread = std::thread(&Message_Stack::Main_Process,this);
}

void Message_Stack::Save_Log()
{
	this->Log_name;
	std::ofstream outputfile;
	time_t timer;
	tm* date;
	timer = time(nullptr); // 経過時間を取得
	date = localtime(&timer); // 経過時間を時間を表す構造体 date に変換
	outputfile.open(this->Log_name, std::ios::out);
	outputfile << "\nFBXコンバートログファイル\n\n";
	outputfile << "出力時間[" << date->tm_year + 1900 << "年" << date->tm_mon + 1 << "月" << date->tm_mday << "日";
	outputfile << date->tm_hour << "時" << date->tm_min << "分" << date->tm_sec << "秒]\n";

	outputfile << "\n＜＜ログスタート＞＞\n";
	for (auto&& i : this->Stack)
	{
		outputfile << i;
	}
	outputfile << "\n＜＜ログエンド＞＞\n";
	outputfile << "Exit\n";
	outputfile.close();
}

void Message_Stack::SetMessage(std::string in)
{
	{
		for (;!mtx.try_lock(););
		Set_Queue.push(in);
		mtx.unlock();
	}
}

void Message_Stack::End()
{
	for (; !mtx.try_lock();) ;
	mtx.unlock();
	this->thread.detach();
}

void Message_Stack::Main_Process()
{
	for (;;)
	{
		{
			for (; !mtx.try_lock(););
			if (!this->Set_Queue.empty())
			{
				this->Stack.push_back(this->Set_Queue.front());
				this->Set_Queue.pop();
			}
			mtx.unlock();
		}
	}
}
