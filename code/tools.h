#pragma once
#include <string>
#include <stdio.h>
#include "mfm.hpp"
#include "mfv.hpp"

class tools
{
public:
	//フルパスからファイルまでのパスを返す
	static void cat_file_path(std::string* Out, std::string In);

	//フルパスからファイル名を抽出する
	static void get_file_name(std::string* out, std::string* in);

	//ファイルの拡張子を変更する
	static void change_file_extension(std::string* Out, std::string* In, std::string* Extension);

	//cat_file_pathにファイル名を追加し拡張子を変更する　出力用のファイルパスを作るときに使う
	static std::string path_editer(std::string* File_dir, std::string* Full_path, const char* SetExtension);

	//パス先からフォルダがあるかを調べる、なければ作る
	static void check_export_folder(std::string* out_path, std::string* out_dir, const char* FolderName);

	//0チェック
	static bool zero_check(const int valu,const char* out_sritng);

	//モデルファイルのコンバータ
	static void change_vertex_data(mfm::model* in_data, mfv::model* out_data);
};

