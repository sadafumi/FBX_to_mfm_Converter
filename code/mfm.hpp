//-----------------------------------------------------------------------------
// AT12A242 宗貞史樹　2017/6/27 
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// メインヘッダ			　　
//-----------------------------------------------------------------------------
#ifndef __MFM_H__
#define __MFM_H__

#include <Windows.h>
#include <stdio.h>
#include <vector>
#include <string>

#define BONE_MAX (4) //DirectX9用のもの

namespace mfm
{
	class float3
	{
	public:
		float x;
		float y;
		float z;
	};

	class uv
	{
	public:
		float u;
		float v;
	};

	class matrix
	{
	public:
		void set(int r, int c, float in) { element[r][c] = in; };
		float get(int r, int c) { return element[r][c]; };
	private:
		float element[4][4];
	};

	class animation_list
	{
	public:
		matrix init;
		std::vector<matrix> frame;
	};
	class bone
	{
	public:
		std::vector<float> weight;
		std::vector<int> index;
	};
	class material
	{
	public:
		std::string name;
	};
	
	class animation
	{
	public:
		int frame;
		std::string name;
		std::vector<animation_list> matrixs;
	};
	class mesh
	{
	public:
		std::vector<float3> vertex;
		std::vector<float3> normals;
		std::vector<uv> uv;
		std::vector<int> index;
		std::vector<bone> bones;
		int material_index;
		int primitive_index;
	private:
	};

	//ファイルのioに使うクラス
	//関数として定義して関数でioを行う
	class model
	{
	public:
		std::vector<mesh>		meshs;		
		std::vector<material>	material;	
		std::vector<animation>	animations;
		bool Is_Animation() 
		{
			return animations.size() != 0;
		};
		bool File_Save_Mesh_Data(const char* filename) 
		{
			FILE* fp = NULL;

			if ((fp = fopen(filename, "wb")) == NULL)
			{
				printf("ファイル(%s)が開けませんでした\n", filename);
				return true;
			}
			int mesh_size = (int)meshs.size();
			fwrite(&mesh_size, sizeof(int), 1, fp);
			for (size_t i = 0; i < meshs.size(); i++)
			{
				Write_Mesh(&meshs[i], fp);
			}
			int material_size = (int)material.size();
			fwrite(&material_size, sizeof(int), 1, fp);
			for (size_t i = 0; i < material.size(); i++)
			{
				fwrite(this->material[i].name.data(), sizeof(char), MAX_PATH, fp);
			}

			fclose(fp);
			return false;
		};
		bool File_Load_Mesh_Data(const char* filename) 
		{
			FILE* fp = NULL;

			if ((fp = fopen(filename, "rb")) == NULL)
			{
				printf("ファイルが開けませんでした\n");
				return true;
			}
			int mesh_size;
			fread(&mesh_size, sizeof(int), 1, fp);
			meshs.resize(mesh_size);
			for (size_t i = 0; i < meshs.size(); i++)
			{
				Read_Mesh(&meshs[i], fp);
			}
			int material_size;
			fread(&material_size, sizeof(int), 1, fp);
			material.resize(material_size);
			for (size_t i = 0; i < material.size(); i++)
			{
				char* name = new char[MAX_PATH];
				fread(name, sizeof(char), MAX_PATH, fp);
				this->material[i].name.append(name);
				delete[] name;
			}
			fclose(fp);
			return false;
		};
		bool File_Save_Full_Data(const char* filename) 
		{
			FILE* fp = NULL;
			if ((fp = fopen(filename, "wb")) == NULL)
			{
				printf("ファイル(%s)が開けませんでした\n", filename);
				return true;
			}
			int mesh_size = (int)meshs.size();
			fwrite(&mesh_size, sizeof(int), 1, fp);
			for (size_t i = 0; i < meshs.size(); i++)
			{
				Write_Mesh(&meshs[i], fp);
				Write_Bone(&meshs[i], fp);
			}
			int material_size = (int)material.size();
			fwrite(&material_size, sizeof(int), 1, fp);
			for (size_t i = 0; i < material.size(); i++)
			{
				fwrite(this->material[i].name.data(), sizeof(char), MAX_PATH, fp);
			}
			int animation_arrey_size = (int)this->animations.size();
			fwrite(&animation_arrey_size, sizeof(size_t), 1, fp);
			if (animation_arrey_size != 0)
			{
				for (int i = 0; i < animation_arrey_size; i++)
				{
					Write_Animation(&this->animations[i], fp);
				}
			}
			fclose(fp);
			return false;
		};
		bool File_Load_Full_Data(const char* filename) 
		{
			FILE* fp = NULL;
			if ((fp = fopen(filename, "rb")) == NULL)
			{
				printf("ファイルが開けませんでした\n");
				return true;
			}
			int mesh_size;
			fread(&mesh_size, sizeof(int), 1, fp);
			meshs.resize(mesh_size);
			for (size_t i = 0; i < meshs.size(); i++)
			{
				Read_Mesh(&meshs[i], fp);
				Read_Bone(&meshs[i], fp);
			}
			int material_size;
			fread(&material_size, sizeof(int), 1, fp);
			material.resize(material_size);
			for (size_t i = 0; i < material.size(); i++)
			{
				char* name = new char[MAX_PATH];
				fread(name, sizeof(char), MAX_PATH, fp);
				this->material[i].name.append(name);
				delete[] name;
			}
			int animation_arrey_size;
			fread(&animation_arrey_size, sizeof(size_t), 1, fp);
			if (animation_arrey_size != 0)
			{
				this->animations.resize(animation_arrey_size);
				for (int i = 0; i < animation_arrey_size; i++)
				{
					Read_Animation(&this->animations[i], fp);
				}
			}
			fclose(fp);
			return false;
		};
	private:
		bool Write_Mesh(mesh* data, FILE* fp) 
		{
			fwrite(&data->material_index, sizeof(int), 1, fp);
			fwrite(&data->primitive_index, sizeof(int), 1, fp);

			int vertex_arrey_size = (int)data->vertex.size();
			fwrite(&vertex_arrey_size, sizeof(size_t), 1, fp);
			fwrite(data->vertex.data(), sizeof(float3), vertex_arrey_size, fp);

			int index_arrey_size = (int)data->index.size();
			fwrite(&index_arrey_size, sizeof(int), 1, fp);
			fwrite(data->index.data(), sizeof(int), index_arrey_size, fp);

			int normal_arrey_size = (int)data->normals.size();
			fwrite(&normal_arrey_size, sizeof(int), 1, fp);
			fwrite(data->normals.data(), sizeof(float3), normal_arrey_size, fp);

			int uv_arrey_size = (int)data->uv.size();
			fwrite(&uv_arrey_size, sizeof(int), 1, fp);
			fwrite(data->uv.data(), sizeof(uv), uv_arrey_size, fp);
			return false;
		};
		bool Write_Bone(mesh* data, FILE* fp)
		{
			int bones_arrey_size = (int)data->bones.size();
			fwrite(&bones_arrey_size, sizeof(int), 1, fp);
			for (int i = 0; i < bones_arrey_size; i++)
			{
				int index_arrey_size = (int)data->bones[i].index.size();
				fwrite(&index_arrey_size, sizeof(int), 1, fp);
				fwrite(data->bones[i].index.data(), sizeof(int), index_arrey_size, fp);
				int weight_arrey_size = (int)data->bones[i].index.size();
				fwrite(&weight_arrey_size, sizeof(int), 1, fp);
				fwrite(data->bones[i].weight.data(), sizeof(float), weight_arrey_size, fp);
			}
			return false;
		};
		bool Read_Mesh(mesh* data, FILE* fp) 
		{
			fread(&data->material_index, sizeof(int), 1, fp);
			fread(&data->primitive_index, sizeof(int), 1, fp);

			int vertex_arrey_size;
			fread(&vertex_arrey_size, sizeof(size_t), 1, fp);
			data->vertex.resize(vertex_arrey_size);
			fread(data->vertex.data(), sizeof(float3), vertex_arrey_size, fp);

			int index_arrey_size;
			fread(&index_arrey_size, sizeof(int), 1, fp);
			data->index.resize(index_arrey_size);
			fread(data->index.data(), sizeof(int), index_arrey_size, fp);

			int normal_arrey_size;
			fread(&normal_arrey_size, sizeof(int), 1, fp);
			data->normals.resize(normal_arrey_size);
			fread(data->normals.data(), sizeof(float3), normal_arrey_size, fp);

			int uv_arrey_size;
			fread(&uv_arrey_size, sizeof(int), 1, fp);
			data->uv.resize(uv_arrey_size);
			fread(data->uv.data(), sizeof(uv), uv_arrey_size, fp);
			return false;
		};
		bool Read_Bone(mesh* data, FILE* fp)
		{
			int bones_arrey_size;
			fread(&bones_arrey_size, sizeof(int), 1, fp);
			data->bones.resize(bones_arrey_size);
			for (int i = 0; i < bones_arrey_size; i++)
			{
				int index_arrey_size = (int)data->bones[i].index.size();
				fread(&index_arrey_size, sizeof(int), 1, fp);
				data->bones[i].index.resize(index_arrey_size);
				fread(data->bones[i].index.data(), sizeof(int), index_arrey_size, fp);

				int weight_arrey_size = (int)data->bones[i].weight.size();
				fread(&weight_arrey_size, sizeof(int), 1, fp);
				data->bones[i].weight.resize(weight_arrey_size);
				fread(data->bones[i].weight.data(), sizeof(float), weight_arrey_size, fp);
			}
			return false;
		};
		bool Write_Animation(animation* data, FILE* fp)
		{
			fwrite(&data->frame, sizeof(int), 1, fp);
			fwrite(data->name.data(), sizeof(char), MAX_PATH, fp);
			int animation_list_arrey_size = (int)data->matrixs.size();
			fwrite(&animation_list_arrey_size, sizeof(int), 1, fp);
			for (int i = 0; i < animation_list_arrey_size; i++)
			{
				fwrite(&data->matrixs[i].init, sizeof(matrix), 1, fp);
				fwrite(data->matrixs[i].frame.data(), sizeof(matrix), data->frame, fp);
			}
			return false;
		};
		bool Read_Animation(animation* data, FILE* fp)
		{
			fread(&data->frame, sizeof(int), 1, fp);
			char* name = new char[MAX_PATH];
			fread(name, sizeof(char), MAX_PATH, fp);
			data->name.append(name);
			delete[] name;
			int animation_list_arrey_size;
			fread(&animation_list_arrey_size, sizeof(int), 1, fp);
			data->matrixs.resize(animation_list_arrey_size);
			for (int i = 0; i < animation_list_arrey_size; i++)
			{
				fread(&data->matrixs[i].init, sizeof(matrix), 1, fp);
				data->matrixs[i].frame.resize(data->frame);
				fread(data->matrixs[i].frame.data(), sizeof(matrix), data->frame, fp);
			}
			return false;
		};
	};
}


#endif //__MFM_H__
