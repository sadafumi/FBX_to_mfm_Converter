#include "pch.h"


bool Get_Fbx_Mesh(FbxScene* scene, mfm::model* Mesh)
{
	// シーンに含まれるメッシュの解析
	auto meshCount = scene->GetSrcObjectCount<FbxMesh>();
	
	Message_Stack::Set  << "メッシュの数(" << (int)meshCount << ")\n";
	Mesh->meshs.resize(meshCount);
	for (int i = 0; i < meshCount; ++i)
	{
		auto* mesh = scene->GetSrcObject<FbxMesh>(i);
		Message_Stack::Set  << "メッシュ名[" << std::string(mesh->GetName()) << "]";
		Set_Mesh_Date(&Mesh->meshs[i], mesh);
		Get_Fbx_Bones(scene, mesh, &Mesh->meshs[i]);
	}
	return false;
}
bool Get_Fbx_Bones(FbxScene* scene, FbxMesh* inMesh, mfm::mesh* Mesh)
{
	Mesh = Mesh;
	scene = scene;
	//// スキンの数を取得
	int skin_count = inMesh->GetDeformerCount(FbxDeformer::eSkin);

	//skin->GetClusterCount();//クラスターと言ってるけどボーンって認識でok
	//Message_Stack::Set  << "スキン数" << skin_count << "\n";
	if (skin_count == 0)
	{
		Message_Stack::Set  << "スキンがなかった\n";
		return true;
	}
	for (int i = 0; i < skin_count; ++i)
	{
		// i番目のスキンを取得
		FbxSkin* skin = (FbxSkin*)inMesh->GetDeformer(i, FbxDeformer::eSkin);
		if (Get_Bone_Data(skin, Mesh)) return true;
	}
	return false;
}
bool Get_Bone_Data(FbxSkin* in_skin, mfm::mesh* Mesh)
{
	// クラスターの数を取得
	int cluster_size = in_skin->GetClusterCount();
	Message_Stack::Set  << "クラスター(ボーン)数[" << cluster_size << "]\n";
	if (cluster_size == 0)
	{
		return true;
	}
	Mesh->bones.resize(Mesh->vertex.size());//DX9でmeshのサイズだけ作ります
	for (int i = 0; i < cluster_size; i++)
	{
		FbxCluster* bone = in_skin->GetCluster(i);
		int	point_size = bone->GetControlPointIndicesCount();
		if (point_size != 0)
		{
			//Message_Stack::Set  << "ボーン番号[" << i << "]に影響する頂点数(" << point_size << ")\n";
		}
		if(Get_Point_Data(bone,Mesh,i)) return true;
	}
	return false;
}
bool Get_Point_Data(FbxCluster* in_bone, mfm::mesh* Mesh,int cluster_index)
{
	int	point_size = in_bone->GetControlPointIndicesCount();
	int* pointAry = in_bone->GetControlPointIndices();
	double* weightAry = in_bone->GetControlPointWeights();
	for (int i = 0; i < point_size; i++)
	{
		Mesh->bones[pointAry[i]].index.push_back(cluster_index);
		Mesh->bones[pointAry[i]].weight.push_back((float)weightAry[i]);
		//Message_Stack::Set  << "頂点番号[" << i << "]インデックス(" << pointAry[i] << ")weight値(" << std::to_string(weightAry[i]) << ")\n";
	}
	return false;
}
bool Set_Mesh_Date(mfm::mesh* in_data, FbxMesh* fbx_mesh)
{
	int MaterialNum;
	int* IndexAry = fbx_mesh->GetPolygonVertices();			//インデックス配列のポインタ
	FbxVector4* src = fbx_mesh->GetControlPoints();			// 頂点座標配列

	FbxArray<FbxVector4> normals;
	fbx_mesh->GetPolygonVertexNormals(normals);

	FbxStringList uvsetName;
	fbx_mesh->GetUVSetNames(uvsetName);
	FbxArray<FbxVector2> uvsets;
	fbx_mesh->GetPolygonVertexUVs(uvsetName.GetStringAt(0), uvsets);

	in_data->uv.resize(uvsets.Size());
	if (tools::zero_check(uvsetName.GetCount(), "インデックスがありません"))	return true;
	for (int i = 0; i < (int)uvsets.Size(); i++)
	{
		in_data->uv[i].u = (float)uvsets[i][0];
		in_data->uv[i].v = 1.0f - (float)uvsets[i][1];
	}
	in_data->index.resize(fbx_mesh->GetPolygonVertexCount());
	if (tools::zero_check(fbx_mesh->GetPolygonVertexCount(), "インデックスがありません"))	return true;
	for (int i = 0; i < fbx_mesh->GetPolygonVertexCount(); i++)
	{
		in_data->index[i] = IndexAry[i];
	}
	if (tools::zero_check(fbx_mesh->GetControlPointsCount(), "頂点がありません"))	return true;
	in_data->vertex.resize(fbx_mesh->GetControlPointsCount());
	for (int i = 0; i < fbx_mesh->GetControlPointsCount(); i++)
	{
		in_data->vertex[i].x = (float)src[i][0];
		in_data->vertex[i].y = (float)src[i][1];
		in_data->vertex[i].z = (float)src[i][2];
	}

	in_data->normals.resize(normals.Size());	
	if (tools::zero_check((int)normals.Size(), "法線がありません"))	return true;
	for (int i = 0; i < (int)normals.Size(); i++)
	{
		in_data->normals[i].x = (float)normals[i][0];
		in_data->normals[i].y = (float)normals[i][1];
		in_data->normals[i].z = (float)normals[i][2];
	}

	//FbxProperty prop = material->FindProperty(FbxSurfaceMaterial::sAmbient);

	FbxLayerElementMaterial* Material = fbx_mesh->GetLayer(0)->GetMaterials();
	//if( Material->GetMappingMode() == FbxLayerElement::eAllSame)
	if (Material != nullptr)
	{
		if (Material->GetMappingMode() == FbxLayerElement::eByPolygonVertex)
		{
			MaterialNum = 0;
			//Message_Stack::Set  << "Material Number(" << MaterialNum << ")\n";
		}
		else
		{
			MaterialNum = Material->GetIndexArray().GetAt(0);
			//Message_Stack::Set  << "Material Number(" << MaterialNum << ")\n";
		}

		in_data->material_index = MaterialNum;
	}
	in_data->primitive_index = fbx_mesh->GetPolygonCount();
	return false;
}