#pragma once
#include <fbxsdk.h>
#include "mfm.hpp"

bool Get_Fbx_Animation(FbxScene* scene, mfm::model* Mesh);
//void GetTimeSettings(FbxScene *Scene, mfm::animation* model);

bool Get_Time(mfm::animation* out_animation, FbxTakeInfo* in_info, FbxTime delta_time);
void Set_Name(mfm::animation* out_animation, FbxTakeInfo* in_info);
int Set_Frame(FbxTakeInfo* in_info, FbxTime delta_time);
bool Get_Animation(mfm::animation* out_animation,FbxMesh* inMesh);
void Set_Matrix(mfm::animation_list* out_animation, int frame, FbxCluster* in_bone);
