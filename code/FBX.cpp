#include "pch.h"

bool FBX::CreateFbxManager(FbxManager** manager)
{
	*manager = FbxManager::Create();

	return *manager != nullptr;
}
bool FBX::CreateFbxImporter(FbxManager** manager, FbxImporter** importer)
{
	*importer = FbxImporter::Create(*manager, "");

	return *importer != nullptr;
}
bool FBX::CreateFbxScene(FbxManager** manager, FbxScene** scene)
{
	*scene = FbxScene::Create(*manager, "");

	return *scene != nullptr;
}

bool FBX::load_fbx(std::string* flie_link, mfm::model* mesh)
{
	FbxManager* manager = nullptr;
	FbxImporter* importer = nullptr;
	FbxScene* scene = nullptr;

	// FBXの設定
	if (!CreateFbxManager(&manager))					return true;
	if (!CreateFbxImporter(&manager, &importer))		return true;
	if (!CreateFbxScene(&manager, &scene))	return true;


	if (!importer->Initialize(flie_link->data(), -1, manager->GetIOSettings())) return true;
	importer->Import(scene);
	FbxGeometryConverter geometryConverter(manager);
	geometryConverter.Triangulate(scene, true);
	Message_Stack::Set << "ポリゴンの三頂点化\n";
	geometryConverter.SplitMeshesPerMaterial(scene, true);
	Message_Stack::Set << "マテリアルごとにメッシュを分割\n";

	if (Get_Fbx_Mesh(scene, mesh))		return true;
	if (Get_Fbx_Material(scene, mesh))	return true;
	if (Get_Fbx_Animation(scene, mesh))	return true;

	Message_Stack::Set << "==========================================================================================\n";
	Message_Stack::Set << "使用されているテクスチャ\n";
	for (size_t i = 0; i < (size_t)mesh->material.size(); i++)
	{
		Message_Stack::Set << "Material番号("<< i <<")テクスチャ名["<< mesh->material[i].name.data() <<"]\n";
	}
	Message_Stack::Set << "==========================================================================================\n";
	

	return false;
}

